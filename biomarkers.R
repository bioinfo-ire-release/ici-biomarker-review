library(data.table)
source("ips.R")
library(pROC)
library(R.utils)

visualize.plots <- T

marker.combinations <- F

auc.value.threshold <- 0.6

save.plot <- F

filter.by.best.rocs <- T

if (marker.combinations){
  filter.by.best.rocs <- F
}

print.plot <- T
bar.plot <- F


write.tables <- T
write.marker.tables <- T

#1: CD274 expression; 2: mutational burden; 3: IFN-g; 4: Expanded related immune signature; 5: IPS; 6: PDCD1 expression; 7: POLE expression; 8: POLE2 expression; 9: POLE3 expression; 10: POLE4 expression; 11: CTLA4 expression; 12: PDCD1LG2 expression; 13: ICB resistance signature 1; 14: ICB resistance signature 2; 15: ICB resistance signature 3; 16: AXL pathway; 17: AXL expression; 18: TIDE

c.analysis <- 1:18

if (marker.combinations){
  library(SDMTools) 
  library(mltools)
  library(easyGgplot2)
  library(reshape2)
  library(gplots)
  
  get_upper_tri <- function(cormat){
    cormat[lower.tri(cormat)]<- NA
    return(cormat)
  }
  
  filter.by.best.rocs <- F
  compute.glm <- T
  
  compute.correlations <- T
  combine.predictors <- T

  violinplot.glm <- T
  save.plot <- F
  
  correlations.heatmap <- T
  plot.combinations.heatmap <- T
  
} else {
  if (save.plot){
    print.plot <- T
  }
  
  compute.glm <- F
  
  combine.predictors <- F
  compute.correlations <- F
  violinplot.glm <- F
  
  correlations.heatmap <- F
  
  plot.combinations.heatmap <- F
}    

  
if (18 %in% c.analysis){
  dt.dys.scores.core <- read.table(file.path("data", "dysfunction_scores_t_dysfunction.tsv"), header = T, stringsAsFactors = F)
  dt.exc.scores <- read.delim(file.path("data", "exclusion_scores.tsv"), header = T, stringsAsFactors = F)
  dt.exc.scores$Gene <- rownames(dt.exc.scores)
  dt.exc.scores <- dt.exc.scores[,c("Gene", "Mean")]
  
  # standard deviation of Dysfunction signature computed by "Results/Tumor_Dysf_Excl_scores/TCGA.SKCM.RNASeq.OS_primary"
  sd.dys.sign <- 0.1413983
  # correction factor of dysfunction signature:
  cor.fact.dys <- sqrt(sd.dys.sign) / 2
}
  

dt.ci.all <- read.table(gunzip(file.path("data", "clinical_information_all.tsv.gz")), header = T, stringsAsFactors = F)
dt.exome <- read.table(gunzip(file.path("data", "exome_mutations_all.tsv.gz")), header = T, stringsAsFactors = F)
dt.rna <- read.table(gunzip(file.path("data", "rnaseq_all.tsv.gz")), header = T, stringsAsFactors = F, row.names = 1)
  
dt.rna.t <- transpose(dt.rna)
colnames(dt.rna.t) <- rownames(dt.rna)
dt.rna.t$ID <- colnames(dt.rna)

studies <- unique(dt.ci.all$Study)
  
colors.plot <- colorRampPalette(c("blue", "red", "green"))(n = length(c.analysis))
  
dt.markers <- data.frame(marker=character(), AUC=numeric(), study=character(), tumor.type=character(), data.type=character(), nr.positive=numeric(), nr.negative=numeric(), nr.total=numeric(), best.thresh=numeric(), stringsAsFactors = F)
dt.markers.auc.statistics <- data.frame(marker=character(), nr.studies=numeric(), AUC.mean=numeric(), AUC.median=numeric(), AUC.min=numeric(), AUC.max=numeric(), stringsAsFactors = F)
  
dt.conf.mat <- data.frame(markers=character(), nr.markers=numeric(), study=character(), TP=numeric(), FP=numeric(), FN=numeric(), TN=numeric(), sens=numeric(), spec=numeric(), AUC=numeric(), prop.correct=numeric(), MCC=numeric(), stringsAsFactors = F)
dt.conf.mat.nc.all <- data.frame(markers=character(), nr.markers=numeric(), TP=numeric(), FP=numeric(), FN=numeric(), TN=numeric(), sens=numeric(), spec=numeric(), AUC=numeric(), stringsAsFactors = F)

c.no <- c("PD", "NB")
c.yes <- c("PR", "CR", "SD", "LB")

for (i_study in 1:length(studies)){
  dt.roc.info <- data.frame(curve.name=character(), AUC=numeric(), color=character(), best.thresh=numeric(), stringsAsFactors = F)
  
  study <- studies[i_study]
  dt.ci.study <- dt.ci.all[which(dt.ci.all$Study == study),]

  
  if ("OS" %in% colnames(dt.ci.study)){
      dt.ci.study <- dt.ci.study[which(dt.ci.study$OS >= 730 | dt.ci.study$Response %in% c(c.yes,c.no)), c("ID", "Response", "Type", "OS")]
  } else {
      dt.ci.study <- dt.ci.study[which(dt.ci.study$Response %in% c(c.yes,c.no)), c("ID", "Response", "Type")]
  }
    
  if (nrow(dt.ci.study)>0){
    dt.ci.study$Group <- 0
    
    if ("OS" %in% colnames(dt.ci.study)){
      dt.ci.study[which(dt.ci.study$OS >= 730 | dt.ci.study$Response %in% c.yes), "Group"] <- T
    } else {
      dt.ci.study[which(dt.ci.study$Response %in% c.yes), "Group"] <- T
    }

    first.analysis <- T
    i.analysis <- 0
    for (analysis in c.analysis){
      i.analysis <- i.analysis + 1
      
      if (analysis %in% c(1,6:12, 17)){ 
        dt.mrg <- merge(dt.ci.study, dt.rna.t)
        
        if (analysis == 1){
          gene.to.select <- "CD274"
        } else if (analysis == 6){
          gene.to.select <- "PDCD1"
        } else if (analysis == 7){
          gene.to.select <- "POLE"
        } else if (analysis == 8){
          gene.to.select <- "POLE2"
        } else if (analysis == 9){
          gene.to.select <- "POLE3"
        } else if (analysis == 10){
          gene.to.select <- "POLE4"
        } else if (analysis == 11){
          gene.to.select <- "CTLA4"
        } else if (analysis == 12){
          gene.to.select <- "PDCD1LG2"
        } else if (analysis == 17){
          gene.to.select <- "AXL"
        }
        
        dt.mrg.red <- dt.mrg[,c("ID", "Group", gene.to.select)]
        colnames(dt.mrg.red)[3] <- "predictor"
        
        nr.patients.title <- nrow(dt.mrg.red)
        curve.title <- gene.to.select
        
      } else if (analysis %in% c(2)) {
        c.variant_classification <- c("Missense","StopGain","StopLoss")
        curve.title <- "mut. load"
          
        dt.exome.vc <- dt.exome[which(dt.exome$Variant_Classification %in% c.variant_classification),]
        #   
        dt.freq <- as.data.frame(table(dt.exome.vc$Patient))
        colnames(dt.freq) <- c("ID", "predictor")
        dt.mrg.red <- merge(dt.ci.study, dt.freq, by = "ID")

              } else if (analysis %in% c(3,4,13:16)){
        dt.mrg <- merge(dt.ci.study, dt.rna.t)
        
        if (analysis == 3){
          genes.to.select <- c("IDO1", "CXCL10", "CXCL9", "HLA-DRA", "STAT1", "IFNG")
          curve.title <- "IFN-y (reduced set)"
        } else if (analysis == 4){
          genes.to.select <- c("CD3D", "IDO1", "CIITA", "CD3E", "CCL5", "GZMK", "CD2", "HLA-DRA", "CXCL13", "IL2RG", "NKG7", "HLA-E", "CXCR6", "LAG3", "TAGAP", "CXCL10", "STAT1", "GZMB")
          curve.title <- "IFN-y (expanded set)"
        } else if (analysis == 13){
          curve.title <- "ICB resistance signature 1"
          genes.to.select <- c("AXL", "ROR2", "WNT5A", "LOXL2", "TWIST2", "TAGLN", "FAP", "MMP1", "MMP13", "COL8A1", "COL12A1", "INHBA", "FBLN1", "ADAMTS7")
        } else if (analysis == 14){
          curve.title <- "ICB resistance signature 2"
          genes.to.select <- c("CNN1", "SERPINF2", "COL3A1", "MXRA7")
        } else if (analysis == 15){
          curve.title <- "ICB resistance signature 3"
          genes.to.select <- c("LAMA3", "CST2")
        } else if (analysis == 16){
          curve.title <- "AXL pathway"
          genes.to.select <- c("KDR", "MET", "AXL", "TEK", "MERTK", "TYRO3", "RET", "FLT1", "FLT4", "GAS6")
        }
        
        dt.mrg.prov <- dt.mrg[,c("ID", "Group", genes.to.select)]
        
        log.transformation <- F
        max.normalization <- T
        if ((log.transformation | max.normalization) & nrow(dt.mrg.prov) > 0){
          cols.to.delete <- c()
          cols.sum <- colSums(dt.mrg.prov[,genes.to.select])
          for (i_cols in 1:length(genes.to.select)){
            if (cols.sum[i_cols] == 0){
              cols.to.delete <- c(cols.to.delete, i_cols)
              dt.mrg.prov <- dt.mrg.prov[, which(! colnames(dt.mrg.prov) %in% names(cols.sum)[i_cols])]
            }
          }
          if (! is.null(cols.to.delete)){
            genes.to.select <- genes.to.select[- cols.to.delete]
          }
          
          if (log.transformation){
            dt.mrg.prov[,genes.to.select][which(dt.mrg.prov[,genes.to.select]==0, arr.ind = T)] <- 1e-6
            dt.mrg.prov[,genes.to.select] <- log10(dt.mrg.prov[,genes.to.select])
          } else if (max.normalization){
            dt.cols.max <- as.data.frame(t(apply(dt.mrg.prov[,genes.to.select], 2, function(x) max(x, na.rm = TRUE))))
            
            dt.cols.max <- as.data.frame(lapply(dt.cols.max, rep, nrow(dt.mrg.prov)))
            
            dt.mrg.prov[,genes.to.select] <- round(dt.mrg.prov[,genes.to.select] / dt.cols.max, digits = 4)
          }
        }
        
        dt.mrg.prov$predictor <- rowMeans(dt.mrg.prov[,genes.to.select])
        dt.mrg.prov <- dt.mrg.prov[, which(! colnames(dt.mrg.prov) %in% genes.to.select)]
        
        dt.mrg.red <- dt.mrg.prov
        
        nr.patients.title <- nrow(dt.mrg.red)
        
      } else if (analysis == 5){
        dt.rna.study <- dt.rna[, colnames(dt.rna) %in% dt.ci.study$ID]
        
        if (ncol(dt.rna.study) == 0){
          dt.mrg.red <- data.frame(ID=character())
        } else {
          ips.study <- compute.ips(dt.rna.study, TPM.to.plus.one.and.log2 = F)
          
          dt.mrg.prov <- merge(dt.ci.study[,c("ID", "Group")], ips.study[,c("SAMPLE", "IPS")], by.x = "ID", by.y = "SAMPLE")
          
          colnames(dt.mrg.prov)[3] <- "predictor"
          dt.mrg.red <- dt.mrg.prov
          
          curve.title <- "IPS"
          
          nr.patients.title <- nrow(dt.mrg.red)
        }
      }  else if (analysis == 18){
        dt.mrg <- merge(dt.ci.study, dt.rna.t)
        if (nrow(dt.mrg)>0){
          
          c.CTL.genes <- c("CD8A", "CD8B", "GZMA", "GZMB", "PRF1")
          c.samples.CTL.high <- dt.mrg[dt.mrg$CD8A > 0 & dt.mrg$CD8B > 0 & dt.mrg$GZMA > 0 & dt.mrg$GZMB > 0 & dt.mrg$PRF1 > 0, "ID"]
          c.samples.CTL.low <- dt.mrg[! dt.mrg$ID %in% c.samples.CTL.high, "ID"]
          
          dt.rna.CTL.high <- dt.rna[,colnames(dt.rna) %in% c.samples.CTL.high]
          dt.rna.CTL.low <- dt.rna[,colnames(dt.rna) %in% c.samples.CTL.low]
          dt.rna.CTL.high$Gene <- rownames(dt.rna.CTL.high)
          dt.rna.CTL.low$Gene <- rownames(dt.rna.CTL.low)
          
          #CTL high --> dysfunction
          dt.dys.mrg <- merge(dt.rna.CTL.high, dt.dys.scores.core, by.y = "Symbol", by.x = "Gene")
          
          dt.mrg.dys.corr <- data.frame(ID = character(), p.cor = numeric(), stringsAsFactors = F)
          for (i.col.pat in 2:(ncol(dt.dys.mrg)-1)){
            dt.mrg.dys.corr[nrow(dt.mrg.dys.corr) + 1,] <- c(colnames(dt.dys.mrg)[i.col.pat],  cor.test(dt.dys.mrg[,i.col.pat], dt.dys.mrg[,"T.Dysfunction"])$estimate)
          }
          
          #CTL low --> exclusion
          dt.exc.mrg <- merge(dt.rna.CTL.low, dt.exc.scores, by = "Gene")
          
          dt.mrg.exc.corr <- data.frame(ID = character(), p.cor = numeric(), stringsAsFactors = F)
          for (i.col.pat in 2:(ncol(dt.exc.mrg)-1)){
            dt.mrg.exc.corr[nrow(dt.mrg.exc.corr) + 1,] <- c(colnames(dt.exc.mrg)[i.col.pat],  cor.test(dt.exc.mrg[,i.col.pat], dt.exc.mrg[,"Mean"])$estimate)
          }
          
          dt.mrg.exc.corr$p.cor <- as.numeric(dt.mrg.exc.corr$p.cor)
          dt.mrg.dys.corr$p.cor <- as.numeric(dt.mrg.dys.corr$p.cor)
          
          dt.mrg.dys.corr$p.cor <- dt.mrg.dys.corr$p.cor / sqrt(sd.dys.sign) / 2
          
          dt.mrg.red <- merge(dt.mrg[,c("ID", "Group")], rbind.data.frame(dt.mrg.dys.corr, dt.mrg.exc.corr), by = "ID")
          colnames(dt.mrg.red)[3] <- "predictor"
          dt.mrg.red$predictor <- as.numeric(dt.mrg.red$predictor)
          
          nr.patients.title <- nrow(dt.mrg.red)
          curve.title <- "TIDE"
        } else {
          dt.mrg.red <- data.frame()
        }
      }
      
      condition.to.continue <- T
      if (nrow(dt.mrg.red)==0){
        condition.to.continue <- F
      } else if (length(unique(dt.mrg.red$Group)) == 1){
        condition.to.continue <- F
      }
      
      if (condition.to.continue){
        roc.to.plot <- roc(response = dt.mrg.red$Group, predictor = dt.mrg.red$predictor)
        best.threshold <- roc.to.plot$thresholds[order(roc.to.plot$sensitivities + roc.to.plot$specificities, decreasing = T)[1]]
        auc.study <- round(as.numeric(roc.to.plot$auc), digits = 2)
        
        tumor.type <- dt.ci.study[1,"Type"]
        
        if ((! filter.by.best.rocs) | (auc.study >= auc.value.threshold)){
          add.plot <- T
          if (first.analysis){
            first.analysis <- F
            
            if (study %in% c("Rizvi", "Snyder")){
              nr.patients.title <- nrow(dt.mrg.red)
            }
            
            title.plot <- paste(study, tumor.type, paste(as.character(nr.patients.title), " patients", sep = ""), sep = " - ")
            
            if (save.plot){
              filename.roc <- paste(study, ".png", sep = "")
            }
            
            add.plot <- F
          }
          
          if (print.plot){
            plot(roc.to.plot, main = title.plot, cex.main = 2.5, cex.axis = 1.5, cex.lab = 1.5, col = colors.plot[i.analysis], add = add.plot)
          }
          
          nr.pos <- nrow(dt.mrg.red[which(dt.mrg.red$Group==1),])
          nr.neg <- nrow(dt.mrg.red[which(dt.mrg.red$Group==0),])
          if (analysis != 2){
            data.type <- "RNA"
          } else if (analysis == 2){
            data.type <- "DNA"
          }
          dt.markers[nrow(dt.markers)+1,] <- c(curve.title, auc.study, study, tumor.type, data.type, nr.pos, nr.neg, nr.pos+nr.neg, best.threshold)
          if (analysis==2){
            nr.mut.load.patients <- nrow(dt.mrg.red)
          }
          
          if ((compute.correlations | compute.glm) & (i_study %in% c(1,2,5))){
            if (compute.correlations){
              if (! exists("dt.mrg.all.pred") || nrow(dt.mrg.all.pred)==0){
                dt.mrg.all.pred <- dt.mrg.red[, c("ID", "Group", "predictor")]
              } else {
                #if rows for this study are not yet inserted
                if (nrow(dt.mrg.all.pred[which(dt.mrg.red[1,"ID"] %in% dt.mrg.all.pred$ID),]) == 0){
                  dt.mrg.all.pred$predictor <- NA
                  for (i.new.rows in 1:nrow(dt.mrg.red)){
                    dt.mrg.all.pred[nrow(dt.mrg.all.pred)+1,] <- c(dt.mrg.red[i.new.rows,c("ID", "Group")],  rep(NA, ncol(dt.mrg.all.pred)-3), dt.mrg.red[i.new.rows,"predictor"])
                  }
                } else {
                  dt.mrg.all.pred <- merge(dt.mrg.red[,c("ID", "predictor")], dt.mrg.all.pred, by = "ID", all = T)
                }
              }
              #add new column if not existing
              if (! paste(curve.title, "pred", sep = ".") %in% colnames(dt.mrg.all.pred)){
                dt.mrg.all.pred$new.col <- NA
                if (analysis == 2){
                  curve.title
                }
                colnames(dt.mrg.all.pred)[ncol(dt.mrg.all.pred)] <- paste(curve.title, "pred", sep = ".")
              }
              dt.mrg.all.pred[which(dt.mrg.all.pred[,"predictor"]>=best.threshold & is.na(dt.mrg.all.pred[,paste(curve.title, "pred", sep = ".")])),paste(curve.title, "pred", sep = ".")] <- 1
              dt.mrg.all.pred[which(dt.mrg.all.pred[,"predictor"]< best.threshold & is.na(dt.mrg.all.pred[,paste(curve.title, "pred", sep = ".")])),paste(curve.title, "pred", sep = ".")] <- 0
              dt.mrg.all.pred <- dt.mrg.all.pred[, - which(colnames(dt.mrg.all.pred) == "predictor") ]
            }
            
            if (compute.glm){
              if (! exists("dt.mrg.cont.pred") || nrow(dt.mrg.cont.pred)==0){
                dt.mrg.cont.pred <- dt.mrg.red[, c("ID", "Group", "predictor")]
                colnames(dt.mrg.cont.pred)[ncol(dt.mrg.cont.pred)] <- curve.title
              } else {
                #if rows for this study are not inserted yet
                if (nrow(dt.mrg.cont.pred[which(dt.mrg.red[1,"ID"] %in% dt.mrg.cont.pred$ID),]) == 0){
                  for (i.new.rows in 1:nrow(dt.mrg.red)){
                    dt.mrg.cont.pred[nrow(dt.mrg.cont.pred)+1,] <- c(dt.mrg.red[i.new.rows,c("ID", "Group")],  rep(NA, ncol(dt.mrg.cont.pred)-2))
                  }
                }
                #add new column if not existing
                if (!curve.title %in% colnames(dt.mrg.cont.pred)){
                  dt.mrg.cont.pred$new.col <- NA
                  colnames(dt.mrg.cont.pred)[ncol(dt.mrg.cont.pred)] <- curve.title
                }
                
                for (IDs in dt.mrg.red$ID){
                  dt.mrg.cont.pred[which(dt.mrg.cont.pred$ID == IDs), curve.title] <- dt.mrg.red[which(dt.mrg.red$ID == IDs), "predictor"]
                }
              }
            }
            
            if (! exists("dt.mrg.pred") || nrow(dt.mrg.pred)==0){
              dt.mrg.pred <- dt.mrg.red[, c("ID", "Group", "predictor")]
            } else {
              dt.mrg.pred <- merge(dt.mrg.pred, dt.mrg.red[, c("ID", "predictor")], by = "ID")
            }
            colnames(dt.mrg.pred)[ncol(dt.mrg.pred)] <- curve.title
            
            dt.mrg.pred$new.col <- 0
            colnames(dt.mrg.pred)[ncol(dt.mrg.pred)] <- paste(curve.title, "pred", sep = ".")
            dt.mrg.pred[which(dt.mrg.pred[,curve.title]>=best.threshold),paste(curve.title, "pred", sep = ".")] <- 1
            dt.mrg.pred <- dt.mrg.pred[, - (ncol(dt.mrg.pred)-1)]
          }
          
          dt.roc.info[nrow(dt.roc.info)+1,] <- c(curve.title, auc.study, colors.plot[i.analysis], best.threshold)
        }
      }
    } #for analysis
    
    if (nrow(dt.roc.info)>0){
      dt.roc.info <- dt.roc.info[order(dt.roc.info$AUC, decreasing = T),]
      
      if (nrow(dt.roc.info[dt.roc.info$curve.name == "mut. load",])>0){
        dt.roc.info[dt.roc.info$curve.name == "mut. load", "curve.name"] <- paste("mut.load (", nr.mut.load.patients, " patients)", sep = "")
      }
      legend.curve.title <- paste(dt.roc.info$curve.name, ", AUC=", as.character(dt.roc.info$AUC), sep = "")
      
      if (print.plot){
        legend(fill = dt.roc.info$color, legend = legend.curve.title, cex = 1.5, x = 0.6, y = 0.3)
      }
    }
    
    if (i_study %in% c(1,2,5) && compute.correlations && exists("dt.mrg.pred")){
      final.col <- ncol(dt.mrg.pred)
      
      if (compute.correlations){
        if (! exists("dt.correlations")){
          dt.correlations <- dt.mrg.pred[,2:final.col]
        } else {
          dt.correlations <- rbind(dt.correlations, dt.mrg.pred[,2:final.col])
        }
      }
    }
  } # study > 0
  
  
  
} #for studies

dt.markers$short <- dt.markers$marker
dt.markers[dt.markers$marker == "ICB resistance signature 1", "short"] <- "ICB res.1"
dt.markers[dt.markers$marker == "ICB resistance signature 2", "short"] <- "ICB res.2"
dt.markers[dt.markers$marker == "ICB resistance signature 3", "short"] <- "ICB res.3"
dt.markers[dt.markers$marker == "IFN-y (reduced set)", "short"] <- "IFNy red."
dt.markers[dt.markers$marker == "IFN-y (expanded set)", "short"] <- "IFNy exp."
dt.markers[dt.markers$marker == "AXL pathway", "short"] <- "AXL path."

dt.markers.sep <- dt.markers
for (marker in unique(dt.markers.sep$marker)){
  c.auc.mark <- as.numeric(dt.markers.sep[which(dt.markers.sep$marker == marker), "AUC"])
  dt.markers.auc.statistics[nrow(dt.markers.auc.statistics)+1,] <- c(marker, length(c.auc.mark), round(mean(c.auc.mark), digits = 2), round(median(c.auc.mark), digits = 2), min(c.auc.mark), max(c.auc.mark))
}

dt.markers$AUC <- as.numeric(dt.markers$AUC)
for (single.study in unique(dt.markers$study)){
  dt.marker.study <- dt.markers[dt.markers$study == single.study,c("AUC", "short")]
  if (nrow(dt.marker.study)>1){
    dt.marker.study <- dt.marker.study[order(dt.marker.study$AUC, decreasing = T),]
    dt.marker.study <- dt.marker.study[dt.marker.study$AUC >= auc.value.threshold,]

    png(file.path("output", paste("barplot_markers_", single.study, ".png", sep = "")), width=4400, height=3000, units = 'px', res = 300)
    par(mar=c(6,10,6,16), las = 2)
    x.limits <- c(0, 1.0)
    barplot(dt.marker.study$AUC,  main=single.study, horiz=T, names.arg=dt.marker.study$short, xlab="AUC", col="blue", space=1, xlim=x.limits, cex.main = 2, cex.axis=2,cex.names=2, cex.lab = 2 )
    dev.off()
  }
}



if (write.tables){
  if (compute.correlations){
    write.table(cor(dt.correlations), file.path("output", "correlations.tsv", sep= ""), sep = "\t", row.names = T)
  }
  write.table(x = dt.conf.mat, file = file.path("output", "confusion_matrix.tsv"), sep = "\t", row.names = F)
}

if (write.marker.tables){
  write.table(x = dt.markers, file = file.path("output", "markers.tsv"), sep = "\t", row.names = F)
  write.table(x = dt.markers.auc.statistics, file = file.path("output", "markers_statistics.tsv"), sep = "\t", row.names = F)
}

if (compute.correlations){
  get.correlated.markers <- function(dt.correl, corr.thresh = 0.5){
    correlations <- cor(dt.correl)
    correlations[upper.tri(correlations, diag = T)] = NA
    return(which(correlations>0.5, arr.ind = T))
  }
  
  find.correlations <- function(dt.correl){
    mat.correlated.markers <- get.correlated.markers(dt.correl)
    if (nrow(mat.correlated.markers)>0){
      return(mat.correlated.markers)
    }
    return(NULL)
  }
  
  dt.correlations.reduced <- dt.correlations
  mat.correlated.markers <- find.correlations(dt.correlations.reduced)
  correlated.cols.to.discard <- c()
  
  while (!is.null(mat.correlated.markers)){
    col.most.correlated.by.row <- table(mat.correlated.markers[,1])[order(table(mat.correlated.markers[,1]), decreasing = T)][1]
    
    col.most.correlated.by.col <- table(mat.correlated.markers[,2])[order(table(mat.correlated.markers[,2]), decreasing = T)][1]
    
    if (col.most.correlated.by.row >= col.most.correlated.by.col){
      col.most.correlated <- as.numeric(names(col.most.correlated.by.row))
    } else {
      col.most.correlated <- as.numeric(names(col.most.correlated.by.col))
    }
    
    correlated.cols.to.discard <- c(correlated.cols.to.discard, colnames(dt.correlations.reduced)[col.most.correlated])
    dt.correlations.reduced <- dt.correlations.reduced[,- col.most.correlated]
    
    mat.correlated.markers <- find.correlations(dt.correlations.reduced)
  }
  
  dt.mrg.all.pred <- dt.mrg.all.pred[! is.na(dt.mrg.all.pred$Group),]
  dt.mrg.all.pred <- dt.mrg.all.pred[! is.na(dt.mrg.all.pred$`mut. load.pred`),]
  
  dt.mrg.pred.nc <- dt.mrg.all.pred[, !colnames(dt.mrg.all.pred) %in% correlated.cols.to.discard]
  final.col <- ncol(dt.mrg.pred.nc)
  
  merged.predictors <- F
  
  if (combine.predictors){
    if ((final.col - 2) >= 2){
      for (i.ncomb in 2:(final.col - 2)){
        merged.predictors <- T
        combinations <- combn(colnames(dt.mrg.pred.nc)[3:final.col], i.ncomb)
        
        pos.thresh <- i.ncomb / 2
        for (i.comb in 1:ncol(combinations)){
          c.comb <- combinations[,i.comb]
          comb.collapsed.names <- paste(c.comb, collapse="..")
          
          dt.mrg.pred.nc$new.col <- rowSums(dt.mrg.pred.nc[, c.comb])
          
          obs <- dt.mrg.pred.nc$Group
          pred <- dt.mrg.pred.nc$new.col
          pred[which(pred<pos.thresh)] <- 0
          pred[which(pred>=pos.thresh)] <- 1
          mcc.pred <- mcc(pred, obs)
          conf.mat <- confusion.matrix(obs,pred)
          acc.mat <- accuracy(obs, pred)
          
          colnames(dt.mrg.pred.nc)[ncol(dt.mrg.pred.nc)] <- comb.collapsed.names
          
          dt.conf.mat.nc.all[nrow(dt.conf.mat.nc.all)+1,] <- c(comb.collapsed.names, i.ncomb, conf.mat[2,2], conf.mat[2,1], conf.mat[1,2], conf.mat[1,1], round(acc.mat$sensitivity, 2), round(acc.mat$specificity, 2), round(acc.mat$AUC, 2))#, acc.mat$prop.correct, mcc.pred)
        }
        print(paste(i.ncomb, i.comb, comb.collapsed.names))
      }
    }
    write.table(x = dt.conf.mat.nc.all, file = file.path("output", "confmat_allstudies.tsv"), sep = "\t", row.names = F)
    
    dt.conf.mat.nc.all.add.mark.cols <- dt.conf.mat.nc.all
    
    for (mark in colnames(dt.correlations.reduced)[2:ncol(dt.correlations.reduced)]){
      dt.conf.mat.nc.all.add.mark.cols$new.col <- ""
      colnames(dt.conf.mat.nc.all.add.mark.cols)[ncol(dt.conf.mat.nc.all.add.mark.cols)] <- gsub(x = mark, ".pred", "")
    }
    
    for (i.row in 1:nrow(dt.conf.mat.nc.all.add.mark.cols)){
      c.markers <- unlist(strsplit(gsub(x = dt.conf.mat.nc.all.add.mark.cols[i.row,"markers"], ".pred", ""), "..", fixed = T))
      dt.conf.mat.nc.all.add.mark.cols[i.row, c.markers] <- "X"
    }
    write.table(x = dt.conf.mat.nc.all.add.mark.cols[,c(11:ncol(dt.conf.mat.nc.all.add.mark.cols), 2,8:10)], file = file.path("output", "confmat_withcross_allstudies.tsv"), sep = "\t", row.names = F)
    
    dt.conf.mat.nc.all.add.mark.cols$AUC <- as.numeric(dt.conf.mat.nc.all.add.mark.cols$AUC)
    dt.conf.mat.nc.all.add.mark.cols.filtered.AUC <- dt.conf.mat.nc.all.add.mark.cols[which(dt.conf.mat.nc.all.add.mark.cols$AUC>=0.65), c(1,10:ncol(dt.conf.mat.nc.all.add.mark.cols))]
    c.mark.occurrences.AUCs <- c()
    c.mark.occurrences.names <- c()
    for (i.col.mark in 3:ncol(dt.conf.mat.nc.all.add.mark.cols.filtered.AUC)){
      c.mark.occurrences.names <- c(c.mark.occurrences.names, colnames(dt.conf.mat.nc.all.add.mark.cols.filtered.AUC)[i.col.mark])
      c.mark.occurrences.AUCs <- c(c.mark.occurrences.AUCs, length(grep("X", dt.conf.mat.nc.all.add.mark.cols.filtered.AUC[,i.col.mark])) / nrow(dt.conf.mat.nc.all.add.mark.cols.filtered.AUC))
    }
    
    names(c.mark.occurrences.AUCs) <- c.mark.occurrences.names
    write.table(x = sort(c.mark.occurrences.AUCs, decreasing = T), file = file.path("output", "markers_occurrences_auc_highX_allstudies.tsv"), sep = "\t", row.names = T, col.names = F)
    
    dt.conf.mat.nc.all.add.mark.cols.filtered.sens <- dt.conf.mat.nc.all.add.mark.cols[which(dt.conf.mat.nc.all.add.mark.cols$sens>=0.7), ]
    c.mark.occurrences.sens <- sort(round(colSums(dt.conf.mat.nc.all.add.mark.cols.filtered.sens[,11:ncol(dt.conf.mat.nc.all.add.mark.cols)]) / nrow(dt.conf.mat.nc.all.add.mark.cols.filtered.sens), 2), decreasing = T)
    write.table(x = c.mark.occurrences.sens, file = file.path("output", "markers_occurrences_sens_high_allstudies.tsv"), sep = "\t", row.names = T, col.names = F)
    
    dt.conf.mat.nc.all.add.mark.cols.filtered.spec <- dt.conf.mat.nc.all.add.mark.cols[which(dt.conf.mat.nc.all.add.mark.cols$spec>=0.7), ]
    c.mark.occurrences.spec <- sort(round(colSums(dt.conf.mat.nc.all.add.mark.cols.filtered.spec[,11:ncol(dt.conf.mat.nc.all.add.mark.cols)]) / nrow(dt.conf.mat.nc.all.add.mark.cols.filtered.spec), 2), decreasing = T)
    write.table(x = c.mark.occurrences.spec, file = file.path("output", "markers_occurrences_spec_high_allstudies.tsv"), sep = "\t", row.names = T, col.names = F)
  }
  
  
  if (compute.glm){
    c.AUC.all.markers <- c()
    c.spec.all.markers <- c()
    c.sens.all.markers <- c()
    
    dt.mrg.pred.nc.glm <- dt.mrg.pred.nc[,2:(ncol(dt.correlations.reduced)+1)]
    family.function <- "gaussian"
    fit.for.significant <- glm(Group~., family=family.function, data=dt.mrg.pred.nc.glm) #default family = "gaussian"
    
    write.table(summary(fit.for.significant)$coefficients, file.path("output", "summary_glm.tsv"), sep = "\t", col.names = T, row.names = T)

    for (nr.sampling in 1:10000){
      training.rows <- sort(sample(1:nrow(dt.mrg.pred.nc.glm), round(nrow(dt.mrg.pred.nc.glm) * 0.8)))
      test.rows <- setdiff(1:nrow(dt.mrg.pred.nc.glm), training.rows)
      
      fit.all.nc <- glm(Group~., family=family.function, data=dt.mrg.pred.nc.glm[training.rows,]) #default family = "gaussian"
      rounded.predicted.from.glm.all.nc <- round(predict.glm(fit.all.nc,newdata=dt.mrg.pred.nc.glm[test.rows,], type="response"))
      
      acc.glm.all.nc <- accuracy(pred = rounded.predicted.from.glm.all.nc, obs = dt.mrg.pred.nc.glm[test.rows,"Group"])
      
      
      predicted.from.glm.all.nc <- predict.glm(fit.all.nc,newdata=dt.mrg.pred.nc.glm[test.rows,], type="response")
      roc.from.glm <- roc(response=dt.mrg.pred.nc.glm[test.rows,"Group"], predictor = predicted.from.glm.all.nc)
      sum.spec.plus.sens <- roc.from.glm$specificities + roc.from.glm$sensitivities
      index.best.sens.spec <- order(sum.spec.plus.sens, decreasing = T)[1]
      c.index.best.sens.spec <- which(sum.spec.plus.sens==sum.spec.plus.sens[index.best.sens.spec])
      
      # Case in which the are more than one best sensitivity-specificity sums. 
      # All the possibilities are explored, then the first couple with the min difference between the two values is selected.
      # Without this shrewdness, the couple with the lowest specificity and highest sensitivity was selected,
      # creating an imbalance between the two terms.
      if (length(c.index.best.sens.spec) > 1){
        best.diff.spec.sens <- 1
        for (index.sum in c.index.best.sens.spec){
          diff.spec.sens <- abs(roc.from.glm$specificities[index.sum] - roc.from.glm$sensitivities[index.sum])
          if (diff.spec.sens < best.diff.spec.sens){
            best.diff.spec.sens <- diff.spec.sens
            index.best.diff.spec.sens <- index.sum
          }
        }
        index.best.sens.spec <- index.best.diff.spec.sens
      }
      
      
      c.AUC.all.markers <- c(c.AUC.all.markers, roc.from.glm$auc)
      c.sens.all.markers <- c(c.sens.all.markers, roc.from.glm$sensitivities[index.best.sens.spec])
      c.spec.all.markers <- c(c.spec.all.markers, roc.from.glm$specificities[index.best.sens.spec])
      
    }
    
    mean.auc.glm <- mean(c.AUC.all.markers)
    mean.sens.glm <- mean(c.sens.all.markers)
    mean.spec.glm <- mean(c.spec.all.markers)
    
    colors.statistics <- c("red", "royalblue2", "palevioletred1")
    
    if (violinplot.glm){
      dt.for.vioplot <- data.frame(rep("Sensitivity", 1e4), dt.bind.all.markers$Sensitivity)
      colnames(dt.for.vioplot) <- c("Type", "Values")
      dt.for.vioplot <- rbind(dt.for.vioplot, data.frame(Type = rep("Specificity",1e4), Values = dt.bind.all.markers$Specificity))
      dt.for.vioplot <- rbind(dt.for.vioplot, data.frame(Type = rep("AUC",1e4), Values = dt.bind.all.markers$AUC))
      
      png(file.path("output", "glm_statistics_violinplot.png"), width = 3000, height = 3000, units = 'px', res = 300)
      ggplot2.violinplot(data = dt.for.vioplot, xName='Type',yName='Values', addMean = T, groupName='Type', meanPointColor = "black", groupColors=colors.statistics, xtitle = "", ytitle = "", showLegend = F, mainTitle = "", removePanelGrid = F, removePanelBorder = T, backgroundColor = "white",axisLine=c(0.5, "solid", "black"), gridColor = "black", mainTitleFont=c(25,"bold", "black")) + 
        ggtitle("GLM statistics") + theme(plot.title = element_text(hjust = 0.5))
      dev.off()
    }
  }
}


if (correlations.heatmap){
  colnames(dt.correlations) <- gsub(".pred", "", colnames(dt.correlations))
  melted_cormat <- melt(get_upper_tri(cor(dt.correlations[,-1])), na.rm = T)
  melted_cormat$value <- round(melted_cormat$value,digits = 2)
  
  png(file.path("output", "correlations_heatmap.png"), width = 3000, height = 3000, units = 'px', res = 300)
  ggheatmap <- ggplot(data = melted_cormat, aes(Var2, Var1, fill = value))+
    geom_tile(color = "white")+
    scale_fill_gradient2(low = "blue", high = "red", mid = "white", 
                         midpoint = 0, limit = c(-1,1), space = "Lab", 
                         name="Pearson\nCorrelation") +
    theme(axis.title.x = element_blank(), axis.title.y = element_blank())+
    theme_minimal()+ 
    theme(axis.text.x = element_text(angle = 45, vjust = 1, 
                                     size = 15, hjust = 1), axis.text.y = element_text(vjust = 1, 
                                                                                       size = 15, hjust = 1))+
    coord_fixed()
  
  ggheatmap + 
    geom_text(aes(Var2, Var1, label = value), color = "black", size = 4) +
    theme(
      axis.title.x = element_blank(),
      axis.title.y = element_blank(),
      panel.grid.major = element_blank(),
      # panel.border = element_blank(),
      panel.background = element_blank(),
      axis.ticks = element_blank(),
      legend.justification = c(1, 0),
      legend.position = c(0.6, 0.7),
      legend.direction = "horizontal")+
    guides(fill = guide_colorbar(barwidth = 7, barheight = 1,
                                 title.position = "top", title.hjust = 0.5))
  
  dev.off()
}


if (plot.combinations.heatmap){
  dt.combinations.heatmap <- read.table(file.path("output", "confmat_withcross_allstudies.tsv"), sep = "\t", stringsAsFactors = F, header = T)
  
  dt.combinations.heatmap <- dt.combinations.heatmap[order(dt.combinations.heatmap$AUC, decreasing = T),]
  
  dt.combinations.heatmap.hm <- dt.combinations.heatmap[,c(1:17,21)]
  
  dt.combinations.heatmap.red <- dt.combinations.heatmap.hm[dt.combinations.heatmap.hm$AUC >= 0.65,1:17]
  
  labs.heatmap <- colnames(dt.combinations.heatmap.hm)
  labs.heatmap[2] <- "mut.load"
  labs.heatmap[3] <- "IFN-y red."
  labs.heatmap[4] <- "IFN-y exp."
  labs.heatmap[12] <- "ICB sig.1"
  labs.heatmap[13] <-  "ICB sig.2"
  labs.heatmap[14] <-  "ICB sig.3"
  labs.heatmap[15] <- "AXL pathw."
  labs.heatmap.red <- labs.heatmap[1:17]
  
  palette <- terrain.colors(256)
  png(file.path("output", "combinations_heatmap.png"), width = 3000, height = 3000, units = 'px', res = 300)
  heatmap(data.matrix(dt.combinations.heatmap.hm), labRow = "", labCol = labs.heatmap,  col = palette, scale = "none", Rowv = NA, Colv = NA, main = "Marker combinations")
  dev.off()
  
  png(file.path("output", "reduced_marker_combinations_heatmap.png"), width = 3000, height = 3000, units = 'px', res = 300)
  heatmap(data.matrix(dt.combinations.heatmap.red), cexCol = 1, labRow = "",  labCol = labs.heatmap.red,  col = palette, scale = "none", Rowv = NA, Colv = NA, main = "Best marker combinations (AUC >= 0.65)")
  dev.off()
}