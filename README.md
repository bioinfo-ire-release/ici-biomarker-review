# ici-biomarker-review

The script is used to generate data, figures and plots of the article "Combinations of immuno-checkpoint inhibitors predictive biomarkers only marginally improve their individual accuracy".
It allows to test the efficacy of some biomarkers on five datasets (WES and RNA-seq data correlated with clinical information of patients). At first, biomarkers are taken individually, then they are combined together to check whether there is some improvement in their efficacy.